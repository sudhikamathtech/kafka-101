package com.arkantos;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class Producer101<S, S1> {
    public static void main(String[] args) {
        System.out.println("Hello Kafka 101");

        Properties props = new Properties();
        props.put("bootstrap.servers", "0.0.0.0:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(props);
        for (int i = 0; i < 100; i++) {
            producer.send(new ProducerRecord<String, String>("first_topic", "1", "Hello Producer" + i));
            System.out.println("Message sent to the topic");
        }

        producer.close();
    }

}
